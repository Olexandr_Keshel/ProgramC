﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab22C_
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            matrixADataGridView.RowCount = 1;
            matrixADataGridView.ColumnCount = 1;
            matrixBDataGridView.RowCount = 1;
            matrixBDataGridView.ColumnCount = 1;
        }

        private void RowsNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            matrixADataGridView.RowCount = (int)RowsNumericUpDown.Value;
            matrixBDataGridView.RowCount = (int)RowsNumericUpDown.Value;
        }

        private void ColsNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            matrixADataGridView.ColumnCount = (int)ColsNumericUpDown.Value;
            matrixBDataGridView.ColumnCount = (int)ColsNumericUpDown.Value;
        }
        private void GenerateRandomABToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int numOfRows = matrixADataGridView.RowCount;
            int numOFCols = matrixADataGridView.ColumnCount;
            Matrix A = new Matrix(numOfRows, numOFCols);
            Matrix.WriteMatrixToGrid(matrixADataGridView, A);
            Matrix B = new Matrix(numOfRows, numOFCols);
            Matrix.WriteMatrixToGrid(matrixBDataGridView, B);
        }

        private void GenerateRandomAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int numOfRows = matrixADataGridView.RowCount;
            int numOFCols = matrixADataGridView.ColumnCount;
            Matrix A = new Matrix(numOfRows, numOFCols);
            Matrix.WriteMatrixToGrid(matrixADataGridView, A);
        }

        private void GenerateRandomBToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            int numOfRows = matrixBDataGridView.RowCount;
            int numOFCols = matrixBDataGridView.ColumnCount;
            Matrix B = new Matrix(numOfRows, numOFCols);
            Matrix.WriteMatrixToGrid(matrixBDataGridView, B);
        }

        private void ChangeZeroesOfABtn_Click(object sender, EventArgs e)
        {
            Matrix A = new Matrix(matrixADataGridView);
            Matrix B = new Matrix(matrixBDataGridView);
            A = Matrix.ChangeZerosOfMatrixByAnotherMatrix(A, B);
            MessageBox.Show(A.ToString(), "Matrix A");


        }


    }
}
