﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab22C_
{
    internal class Matrix
    {
        public int [,] data;
        private int Rows;
        private int Cols;

        public Matrix(int numOfRows, int numOfCols)
        {
            Rows = numOfRows;
            Cols = numOfCols;
            data = GetRandomMatrix(Rows, Cols);
        }
        public Matrix(DataGridView grid)
        {
            Rows = grid.RowCount;
            Cols = grid.ColumnCount;
            data = ReadDataFromGrid(grid);
            
        }
        // private indexer
        private double this[int i, int j]
        {
            get
            {
                return data[i, j];
            }
            set
            {
                data[i, j] = (int)value;
            }
        }
        private static int[,] GetRandomMatrix(int numOfRows, int numOfCols)
        {
            Random rnd = new Random(); 
            int [,] _data = new int[numOfRows, numOfCols];
            for(int i = 0; i< numOfRows; i++)
            {
                for(int j = 0; j< numOfCols; j++)
                {
                    _data[i, j] = rnd.Next(-5, 5);
                }
            }
            return _data;
        }
        public static void WriteMatrixToGrid(DataGridView grid, Matrix matrix)
        {
            grid.RowCount = matrix.Rows;
            grid.ColumnCount = matrix.Cols;
            for (int i = 0; i < matrix.Rows; i++)
            {
                for (int j=0; j < matrix.Cols; j++)
                {
                    grid[j, i].Value = matrix[i,j];
                }
            }
        }
        public static int[,] ReadDataFromGrid(DataGridView grid)
        {
            int[,] matrix = new int[grid.RowCount,grid.ColumnCount];
            for (int i = 0; i < grid.RowCount; i++)
            {
                for(int j = 0; j < grid.ColumnCount; j++)
                {
                    matrix[i,j]  = Convert.ToInt32(grid[j, i].Value);

                }
            }
            return matrix;
        }

        public static Matrix ChangeZerosOfMatrixByAnotherMatrix(Matrix matrix1, Matrix matrix2)
        {
            for(int i = 0; i < matrix1.Rows; i++)
            {
                for(int j=0; j < matrix1.Cols; j++)
                {
                    if (matrix1[i,j] == 0)
                    {
                        matrix1[i, j] = matrix2[i,j];
                    }
                }
            }
            return matrix1;
        }


        //override
        public override string ToString()
        {
            string message = " ";
            for (int i = 0; i < this.Rows; i++)
            {
                for (int j = 0; j < this.Cols; j++)
                {
                    message += this[i, j].ToString() + ", ";
                    
                }
                message += "\n";
            }
            return message;
        }
    }
}
