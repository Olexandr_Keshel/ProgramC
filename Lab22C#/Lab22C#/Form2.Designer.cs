﻿namespace Lab22C_
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            matrixADataGridView = new DataGridView();
            matrixBDataGridView = new DataGridView();
            matrixALabel = new Label();
            matrixBLabel = new Label();
            RowsNumericUpDown = new NumericUpDown();
            ColsNumericUpDown = new NumericUpDown();
            rowsLabel = new Label();
            colsLabel = new Label();
            menuStrip1 = new MenuStrip();
            matrixAToolStripMenuItem = new ToolStripMenuItem();
            GenerateRandomAToolStripMenuItem = new ToolStripMenuItem();
            matrixBToolStripMenuItem = new ToolStripMenuItem();
            GenerateRandomBToolStripMenuItem1 = new ToolStripMenuItem();
            bothMatrixToolStripMenuItem = new ToolStripMenuItem();
            GenerateRandomABToolStripMenuItem = new ToolStripMenuItem();
            ChangeZeroesOfABtn = new Button();
            ((System.ComponentModel.ISupportInitialize)matrixADataGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)matrixBDataGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)RowsNumericUpDown).BeginInit();
            ((System.ComponentModel.ISupportInitialize)ColsNumericUpDown).BeginInit();
            menuStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // matrixADataGridView
            // 
            matrixADataGridView.AllowUserToAddRows = false;
            matrixADataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            matrixADataGridView.ColumnHeadersVisible = false;
            matrixADataGridView.Location = new Point(48, 255);
            matrixADataGridView.Name = "matrixADataGridView";
            matrixADataGridView.RowHeadersWidth = 51;
            matrixADataGridView.RowTemplate.Height = 29;
            matrixADataGridView.Size = new Size(508, 357);
            matrixADataGridView.TabIndex = 0;
            // 
            // matrixBDataGridView
            // 
            matrixBDataGridView.AllowUserToAddRows = false;
            matrixBDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            matrixBDataGridView.ColumnHeadersVisible = false;
            matrixBDataGridView.Location = new Point(620, 255);
            matrixBDataGridView.Name = "matrixBDataGridView";
            matrixBDataGridView.RowHeadersWidth = 51;
            matrixBDataGridView.RowTemplate.Height = 29;
            matrixBDataGridView.Size = new Size(508, 357);
            matrixBDataGridView.TabIndex = 1;
            // 
            // matrixALabel
            // 
            matrixALabel.AutoSize = true;
            matrixALabel.Font = new Font("Arial Narrow", 12F, FontStyle.Bold, GraphicsUnit.Point);
            matrixALabel.Location = new Point(48, 228);
            matrixALabel.Name = "matrixALabel";
            matrixALabel.Size = new Size(73, 24);
            matrixALabel.TabIndex = 2;
            matrixALabel.Text = "Matrix A";
            // 
            // matrixBLabel
            // 
            matrixBLabel.AutoSize = true;
            matrixBLabel.Font = new Font("Arial Narrow", 12F, FontStyle.Bold, GraphicsUnit.Point);
            matrixBLabel.Location = new Point(620, 228);
            matrixBLabel.Name = "matrixBLabel";
            matrixBLabel.Size = new Size(74, 24);
            matrixBLabel.TabIndex = 3;
            matrixBLabel.Text = "Matrix B";
            // 
            // RowsNumericUpDown
            // 
            RowsNumericUpDown.Location = new Point(562, 124);
            RowsNumericUpDown.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            RowsNumericUpDown.Name = "RowsNumericUpDown";
            RowsNumericUpDown.Size = new Size(62, 27);
            RowsNumericUpDown.TabIndex = 4;
            RowsNumericUpDown.Value = new decimal(new int[] { 1, 0, 0, 0 });
            RowsNumericUpDown.ValueChanged += RowsNumericUpDown_ValueChanged;
            // 
            // ColsNumericUpDown
            // 
            ColsNumericUpDown.Location = new Point(562, 174);
            ColsNumericUpDown.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            ColsNumericUpDown.Name = "ColsNumericUpDown";
            ColsNumericUpDown.Size = new Size(62, 27);
            ColsNumericUpDown.TabIndex = 6;
            ColsNumericUpDown.Value = new decimal(new int[] { 1, 0, 0, 0 });
            ColsNumericUpDown.ValueChanged += ColsNumericUpDown_ValueChanged;
            // 
            // rowsLabel
            // 
            rowsLabel.AutoSize = true;
            rowsLabel.Location = new Point(457, 124);
            rowsLabel.Name = "rowsLabel";
            rowsLabel.Size = new Size(44, 20);
            rowsLabel.TabIndex = 7;
            rowsLabel.Text = "Rows";
            // 
            // colsLabel
            // 
            colsLabel.AutoSize = true;
            colsLabel.Location = new Point(457, 181);
            colsLabel.Name = "colsLabel";
            colsLabel.Size = new Size(66, 20);
            colsLabel.TabIndex = 8;
            colsLabel.Text = "Columns";
            // 
            // menuStrip1
            // 
            menuStrip1.ImageScalingSize = new Size(20, 20);
            menuStrip1.Items.AddRange(new ToolStripItem[] { matrixAToolStripMenuItem, matrixBToolStripMenuItem, bothMatrixToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(1149, 28);
            menuStrip1.TabIndex = 9;
            menuStrip1.Text = "menuStrip1";
            // 
            // matrixAToolStripMenuItem
            // 
            matrixAToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { GenerateRandomAToolStripMenuItem });
            matrixAToolStripMenuItem.Name = "matrixAToolStripMenuItem";
            matrixAToolStripMenuItem.Size = new Size(79, 24);
            matrixAToolStripMenuItem.Text = "Matrix A";
            // 
            // GenerateRandomAToolStripMenuItem
            // 
            GenerateRandomAToolStripMenuItem.Name = "GenerateRandomAToolStripMenuItem";
            GenerateRandomAToolStripMenuItem.Size = new Size(212, 26);
            GenerateRandomAToolStripMenuItem.Text = "Generate Random";
            GenerateRandomAToolStripMenuItem.Click += GenerateRandomAToolStripMenuItem_Click;
            // 
            // matrixBToolStripMenuItem
            // 
            matrixBToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { GenerateRandomBToolStripMenuItem1 });
            matrixBToolStripMenuItem.Name = "matrixBToolStripMenuItem";
            matrixBToolStripMenuItem.Size = new Size(78, 24);
            matrixBToolStripMenuItem.Text = "Matrix B";
            // 
            // GenerateRandomBToolStripMenuItem1
            // 
            GenerateRandomBToolStripMenuItem1.Name = "GenerateRandomBToolStripMenuItem1";
            GenerateRandomBToolStripMenuItem1.Size = new Size(212, 26);
            GenerateRandomBToolStripMenuItem1.Text = "Generate Random";
            GenerateRandomBToolStripMenuItem1.Click += GenerateRandomBToolStripMenuItem1_Click;
            // 
            // bothMatrixToolStripMenuItem
            // 
            bothMatrixToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { GenerateRandomABToolStripMenuItem });
            bothMatrixToolStripMenuItem.Name = "bothMatrixToolStripMenuItem";
            bothMatrixToolStripMenuItem.Size = new Size(100, 24);
            bothMatrixToolStripMenuItem.Text = "Both Matrix";
            // 
            // GenerateRandomABToolStripMenuItem
            // 
            GenerateRandomABToolStripMenuItem.Name = "GenerateRandomABToolStripMenuItem";
            GenerateRandomABToolStripMenuItem.Size = new Size(212, 26);
            GenerateRandomABToolStripMenuItem.Text = "Generate Random";
            GenerateRandomABToolStripMenuItem.Click += GenerateRandomABToolStripMenuItem_Click;
            // 
            // ChangeZeroesOfABtn
            // 
            ChangeZeroesOfABtn.Location = new Point(476, 654);
            ChangeZeroesOfABtn.Name = "ChangeZeroesOfABtn";
            ChangeZeroesOfABtn.Size = new Size(226, 58);
            ChangeZeroesOfABtn.TabIndex = 10;
            ChangeZeroesOfABtn.Text = "Change Zeroes of Matrix A";
            ChangeZeroesOfABtn.UseVisualStyleBackColor = true;
            ChangeZeroesOfABtn.Click += ChangeZeroesOfABtn_Click;
            // 
            // Form2
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1149, 759);
            Controls.Add(ChangeZeroesOfABtn);
            Controls.Add(colsLabel);
            Controls.Add(rowsLabel);
            Controls.Add(ColsNumericUpDown);
            Controls.Add(RowsNumericUpDown);
            Controls.Add(matrixBLabel);
            Controls.Add(matrixALabel);
            Controls.Add(matrixBDataGridView);
            Controls.Add(matrixADataGridView);
            Controls.Add(menuStrip1);
            MainMenuStrip = menuStrip1;
            Name = "Form2";
            Text = "Form2";
            Load += Form2_Load;
            ((System.ComponentModel.ISupportInitialize)matrixADataGridView).EndInit();
            ((System.ComponentModel.ISupportInitialize)matrixBDataGridView).EndInit();
            ((System.ComponentModel.ISupportInitialize)RowsNumericUpDown).EndInit();
            ((System.ComponentModel.ISupportInitialize)ColsNumericUpDown).EndInit();
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private DataGridView matrixADataGridView;
        private DataGridView matrixBDataGridView;
        private Label matrixALabel;
        private Label matrixBLabel;
        private NumericUpDown RowsNumericUpDown;
        private NumericUpDown ColsNumericUpDown;
        private Label rowsLabel;
        private Label colsLabel;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem matrixAToolStripMenuItem;
        private ToolStripMenuItem GenerateRandomAToolStripMenuItem;
        private ToolStripMenuItem matrixBToolStripMenuItem;
        private ToolStripMenuItem GenerateRandomBToolStripMenuItem1;
        private ToolStripMenuItem bothMatrixToolStripMenuItem;
        private Button ChangeZeroesOfABtn;
        private ToolStripMenuItem GenerateRandomABToolStripMenuItem;
    }
}