﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Lab22C_
{
    internal class VectorFromGrid
    {
        private double[] data;
        private int Length { get { return data.Length; } }
        //constructors
        public VectorFromGrid(DataGridView gridView, int rowIndex)
        {
            data = ReadDataFromGrid(gridView, rowIndex);
        }
        public VectorFromGrid(int length)   
        {
            data = GetRandomVector(length);
        }
        public VectorFromGrid(VectorFromGrid vect1, VectorFromGrid vect2) 
        {

            double[] vect1Pos = GetPositiveElem(vect1);
            double[] vect2Pos = GetPositiveElem(vect2);
            data = new double[vect1Pos.Length + vect2Pos.Length];
            int i = 0;
            while(i < vect1Pos.Length)
            {
                data[i] = vect1Pos[i];
                i++;
            }

            int j = 0;
            int k = i;
            while (j < vect2Pos.Length)
            {
                data[k++] = vect2Pos[j++];
            }

        }
        // private indexer
        private double this[int index]
        {
            get
            {
               if(index >= 0 || index < data.Length) 
                    return data[index];
               else
                    throw new IndexOutOfRangeException();
            }
        }

        //methods
        private static double[] GetRandomVector(int length)
        {
            double[] _data = new double[length];
            Random rnd = new Random();
            for (int i = 0; i < length; i++)
            {
                _data[i] = rnd.Next(-15, 15);
            }
            return _data;
        }

        private static double[] GetPositiveElem(VectorFromGrid vect)
        {
            int amountOfPos = 0;
            
            for(int i=0; i < vect.Length; i++)
            {
                if (vect[i] > 0)
                {
                    amountOfPos++;
                }
            }
            double[] vectPos = new double[amountOfPos];
            int j = 0;
            int k = 0;
            while(j < vect.Length)
            {
                if (vect[j] > 0) 
                {
                    vectPos[k++] = vect[j];

                }
                j++;
            }
            return vectPos;
        }
        public static void WriteTwoVectorsToGrid(DataGridView grid, VectorFromGrid vect1, VectorFromGrid vect2)
        {
            grid.RowCount = 2;
            grid.ColumnCount = vect1.Length;
            for (int i = 0; i < vect1.Length; i++)
            {
                grid[i, 0].Value = vect1[i];
                grid[i, 1].Value = vect2[i];
            }
        }
        public static void WriteVectorToGrid(DataGridView grid, VectorFromGrid vect)
        {
            grid.RowCount = 1;
            grid.ColumnCount = vect.Length;
            for (int i = 0; i < vect.Length; i++)
            {
                grid[i, 0].Value = vect[i];
            }
        }

        //Vector from DataGridView
        public static double[] ReadDataFromGrid(DataGridView grid, int rowIndex)
        {
            double[] vect = new double[grid.ColumnCount];
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                vect[i] = Convert.ToDouble(grid[i, rowIndex].Value); 
            }
            return vect;
        }
    }
}