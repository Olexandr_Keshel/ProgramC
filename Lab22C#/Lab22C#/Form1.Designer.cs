﻿namespace Lab22C_
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            vectLengthLabel = new Label();
            CreateRndVectBtn = new Button();
            vectorsXYGridView = new DataGridView();
            vectorZGridView = new DataGridView();
            label1 = new Label();
            vectLengthNumericUpDown = new NumericUpDown();
            GenerateVectZBtn = new Button();
            ((System.ComponentModel.ISupportInitialize)vectorsXYGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)vectorZGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)vectLengthNumericUpDown).BeginInit();
            SuspendLayout();
            // 
            // vectLengthLabel
            // 
            vectLengthLabel.AutoSize = true;
            vectLengthLabel.Font = new Font("Arial Narrow", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
            vectLengthLabel.Location = new Point(449, 201);
            vectLengthLabel.Name = "vectLengthLabel";
            vectLengthLabel.Size = new Size(275, 27);
            vectLengthLabel.TabIndex = 0;
            vectLengthLabel.Text = "Enter a length of two vectors:\r\n";
            // 
            // CreateRndVectBtn
            // 
            CreateRndVectBtn.Font = new Font("Arial", 12F, FontStyle.Bold, GraphicsUnit.Point);
            CreateRndVectBtn.Location = new Point(62, 296);
            CreateRndVectBtn.Name = "CreateRndVectBtn";
            CreateRndVectBtn.Size = new Size(237, 63);
            CreateRndVectBtn.TabIndex = 1;
            CreateRndVectBtn.Text = "Create random values";
            CreateRndVectBtn.UseVisualStyleBackColor = true;
            CreateRndVectBtn.Click += CreateRndVectBtn_Click;
            // 
            // vectorsXYGridView
            // 
            vectorsXYGridView.AllowUserToAddRows = false;
            vectorsXYGridView.AllowUserToDeleteRows = false;
            vectorsXYGridView.AllowUserToResizeColumns = false;
            vectorsXYGridView.AllowUserToResizeRows = false;
            vectorsXYGridView.BackgroundColor = Color.WhiteSmoke;
            vectorsXYGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            vectorsXYGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            vectorsXYGridView.ColumnHeadersVisible = false;
            vectorsXYGridView.Location = new Point(468, 264);
            vectorsXYGridView.Name = "vectorsXYGridView";
            vectorsXYGridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            vectorsXYGridView.RowHeadersWidth = 51;
            vectorsXYGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            vectorsXYGridView.RowTemplate.Height = 29;
            vectorsXYGridView.ScrollBars = ScrollBars.Horizontal;
            vectorsXYGridView.Size = new Size(588, 123);
            vectorsXYGridView.TabIndex = 3;
            // 
            // vectorZGridView
            // 
            vectorZGridView.BackgroundColor = Color.WhiteSmoke;
            vectorZGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            vectorZGridView.Location = new Point(188, 581);
            vectorZGridView.Name = "vectorZGridView";
            vectorZGridView.RowHeadersWidth = 51;
            vectorZGridView.RowTemplate.Height = 29;
            vectorZGridView.Size = new Size(716, 123);
            vectorZGridView.TabIndex = 4;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(274, 138);
            label1.Name = "label1";
            label1.Size = new Size(0, 20);
            label1.TabIndex = 5;
            // 
            // vectLengthNumericUpDown
            // 
            vectLengthNumericUpDown.Location = new Point(763, 201);
            vectLengthNumericUpDown.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            vectLengthNumericUpDown.Name = "vectLengthNumericUpDown";
            vectLengthNumericUpDown.Size = new Size(150, 27);
            vectLengthNumericUpDown.TabIndex = 6;
            vectLengthNumericUpDown.Value = new decimal(new int[] { 1, 0, 0, 0 });
            vectLengthNumericUpDown.ValueChanged += vectLengthNumericUpDown_ValueChanged;
            // 
            // GenerateVectZBtn
            // 
            GenerateVectZBtn.Font = new Font("Arial", 12F, FontStyle.Bold, GraphicsUnit.Point);
            GenerateVectZBtn.Location = new Point(188, 501);
            GenerateVectZBtn.Name = "GenerateVectZBtn";
            GenerateVectZBtn.Size = new Size(205, 53);
            GenerateVectZBtn.TabIndex = 7;
            GenerateVectZBtn.Text = "Generate vector Z";
            GenerateVectZBtn.UseVisualStyleBackColor = true;
            GenerateVectZBtn.Click += GenerateVectZBtn_Click;
            // 
            // Form3
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1113, 726);
            Controls.Add(GenerateVectZBtn);
            Controls.Add(vectLengthNumericUpDown);
            Controls.Add(label1);
            Controls.Add(vectorZGridView);
            Controls.Add(vectorsXYGridView);
            Controls.Add(CreateRndVectBtn);
            Controls.Add(vectLengthLabel);
            Name = "Form3";
            Text = "Form1";
            Load += Form1_Load;
            ((System.ComponentModel.ISupportInitialize)vectorsXYGridView).EndInit();
            ((System.ComponentModel.ISupportInitialize)vectorZGridView).EndInit();
            ((System.ComponentModel.ISupportInitialize)vectLengthNumericUpDown).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label vectLengthLabel;
        private Button CreateRndVectBtn;
        private DataGridView vectorsXYGridView;
        private DataGridView vectorZGridView;
        private Label label1;
        private NumericUpDown vectLengthNumericUpDown;
        private Button GenerateVectZBtn;
    }
}