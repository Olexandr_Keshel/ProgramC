using System.Runtime.Intrinsics;
using System.Windows.Forms;

namespace Lab22C_
{
    public partial class Form1 : Form
    {


        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            vectorsXYGridView.ColumnCount = 1;
            vectorsXYGridView.RowCount = 2;
            vectorsXYGridView.Rows[0].HeaderCell.Value = "X";
            vectorsXYGridView.Rows[1].HeaderCell.Value = "Y";

            vectorZGridView.ColumnCount = 1;
            vectorZGridView.RowCount = 1;
            vectorZGridView.Rows[0].HeaderCell.Value = "Z";
        }
        private void vectLengthNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            vectorsXYGridView.ColumnCount = (int)vectLengthNumericUpDown.Value;
        }
        private void CreateRndVectBtn_Click(object sender, EventArgs e)
        {
            int length = (int)vectLengthNumericUpDown.Value;
            VectorFromGrid x = new VectorFromGrid(length);
            VectorFromGrid y = new VectorFromGrid(length);
            VectorFromGrid.WriteTwoVectorsToGrid(vectorsXYGridView, x, y);
            VectorFromGrid z = new VectorFromGrid(x, y);
            VectorFromGrid.WriteVectorToGrid(vectorZGridView, z);


        }

        private void GenerateVectZBtn_Click(object sender, EventArgs e)
        {
            int length = (int)vectLengthNumericUpDown.Value;
            VectorFromGrid x = new VectorFromGrid(vectorsXYGridView, 0);
            VectorFromGrid y = new VectorFromGrid(vectorsXYGridView, 1);
            VectorFromGrid z = new VectorFromGrid(x, y);
            VectorFromGrid.WriteVectorToGrid(vectorZGridView, z);


        }
    }
}