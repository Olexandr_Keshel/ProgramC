﻿namespace Lab22._3C_
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.ANumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.aLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.BNmericUpDown = new System.Windows.Forms.NumericUpDown();
            this.bLabel = new System.Windows.Forms.Label();
            this.IntervalNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.inervalLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ANumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BNmericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea6.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.chart1.Legends.Add(legend6);
            this.chart1.Location = new System.Drawing.Point(110, 150);
            this.chart1.Name = "chart1";
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series11.Legend = "Legend1";
            series11.Name = "Series1";
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series12.Legend = "Legend1";
            series12.Name = "Series2";
            this.chart1.Series.Add(series11);
            this.chart1.Series.Add(series12);
            this.chart1.Size = new System.Drawing.Size(983, 391);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // ANumericUpDown
            // 
            this.ANumericUpDown.Location = new System.Drawing.Point(193, 12);
            this.ANumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.ANumericUpDown.Name = "ANumericUpDown";
            this.ANumericUpDown.Size = new System.Drawing.Size(120, 22);
            this.ANumericUpDown.TabIndex = 1;
            this.ANumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            // 
            // aLabel
            // 
            this.aLabel.AutoSize = true;
            this.aLabel.Location = new System.Drawing.Point(136, 18);
            this.aLabel.Name = "aLabel";
            this.aLabel.Size = new System.Drawing.Size(15, 16);
            this.aLabel.TabIndex = 2;
            this.aLabel.Text = "a";
            this.aLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(958, 589);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(196, 92);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BNmericUpDown
            // 
            this.BNmericUpDown.Location = new System.Drawing.Point(193, 56);
            this.BNmericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.BNmericUpDown.Name = "BNmericUpDown";
            this.BNmericUpDown.Size = new System.Drawing.Size(120, 22);
            this.BNmericUpDown.TabIndex = 4;
            this.BNmericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // bLabel
            // 
            this.bLabel.AutoSize = true;
            this.bLabel.Location = new System.Drawing.Point(136, 62);
            this.bLabel.Name = "bLabel";
            this.bLabel.Size = new System.Drawing.Size(15, 16);
            this.bLabel.TabIndex = 5;
            this.bLabel.Text = "b";
            // 
            // IntervalNumericUpDown
            // 
            this.IntervalNumericUpDown.DecimalPlaces = 1;
            this.IntervalNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.IntervalNumericUpDown.Location = new System.Drawing.Point(193, 104);
            this.IntervalNumericUpDown.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.IntervalNumericUpDown.Name = "IntervalNumericUpDown";
            this.IntervalNumericUpDown.Size = new System.Drawing.Size(120, 22);
            this.IntervalNumericUpDown.TabIndex = 6;
            this.IntervalNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // inervalLabel
            // 
            this.inervalLabel.AutoSize = true;
            this.inervalLabel.Location = new System.Drawing.Point(107, 110);
            this.inervalLabel.Name = "inervalLabel";
            this.inervalLabel.Size = new System.Drawing.Size(50, 16);
            this.inervalLabel.TabIndex = 7;
            this.inervalLabel.Text = "Interval";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1156, 693);
            this.Controls.Add(this.inervalLabel);
            this.Controls.Add(this.IntervalNumericUpDown);
            this.Controls.Add(this.bLabel);
            this.Controls.Add(this.BNmericUpDown);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.aLabel);
            this.Controls.Add(this.ANumericUpDown);
            this.Controls.Add(this.chart1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ANumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BNmericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.NumericUpDown ANumericUpDown;
        private System.Windows.Forms.Label aLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown BNmericUpDown;
        private System.Windows.Forms.Label bLabel;
        private System.Windows.Forms.NumericUpDown IntervalNumericUpDown;
        private System.Windows.Forms.Label inervalLabel;
    }
}

