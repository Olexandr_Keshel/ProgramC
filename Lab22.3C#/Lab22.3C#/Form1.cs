﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab22._3C_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            chart1.Series[0].Points.Clear();
            //chart1.Series[1].Points.Clear();
            chart1.ChartAreas[0].AxisX.Minimum = (int)ANumericUpDown.Value;
            chart1.ChartAreas[0].AxisX.Maximum = (int)BNmericUpDown.Value;
            chart1.ChartAreas[0].AxisX.Interval = (double)IntervalNumericUpDown.Value;
            for (double x = chart1.ChartAreas[0].AxisX.Minimum; x <= chart1.ChartAreas[0].AxisX.Maximum; x += chart1.ChartAreas[0].AxisX.Interval)
            {
                chart1.Series[0].Points.AddXY(x, Math.Tan(x) );
            }
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //chart1.Series[0].Points.AddXY(1, 100);
        }
    }
}
