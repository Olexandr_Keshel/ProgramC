using System.Runtime.Intrinsics.Arm;

namespace Lab21C_
{
    public partial class Form1 : Form
    {
        private static double ConvertToDecimal(double numBase, string number)
        { //display on the screen the number given in base from 1 to 10 and 16
            if (numBase <= 10)
            {
                char[] digits = number.ToCharArray();
                double sum = 0;
                Array.Reverse(digits);
                for (int i = 0; i < digits.Length; i++)
                {

                    string digitInStr = Convert.ToString(digits[i]);
                    double digit = Convert.ToDouble(digitInStr);
                    if (digit < numBase)
                    {
                        sum += digit * Math.Pow(numBase, i);
                    }
                    else
                    {
                        throw new Exception("You typed invalid number!");
                    }
                }
                return sum;
            }
            else if (numBase == 10)
            {
                int intValue = Convert.ToInt32(number, 16);
                return Convert.ToDouble(intValue);
            }
            else
            {
                throw new Exception("Sorry, but we can't convert your number in this base");
            }
        }

        public Form1()
        {
            InitializeComponent();
        }


        private void CompareBtn_Click(object sender, EventArgs e)
        {
            double base1 = Convert.ToDouble(base1TextBox.Text);
            string num1 = num1TextBox.Text;
            double base2 = Convert.ToDouble(base2TextBox.Text);
            string num2 = num2TextBox.Text;
            double decimNum1 = ConvertToDecimal(base1, num1);
            double decimNum2 = ConvertToDecimal(base2, num2);

            if (decimNum1 > decimNum2) { compareSighLabel.Text = ">"; }

            else if (decimNum1 < decimNum2) { compareSighLabel.Text = "<"; }

            else { compareSighLabel.Text = "="; }

            decimNum1Label.Text = decimNum1.ToString();
            decimNum2Label.Text = decimNum2.ToString();








        }
    }
}