﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab21C_
{
    public partial class Form2 : Form
    {

        double numerat;
        double denom;

        public Form2()
        {
            InitializeComponent();
        }
        static double Factorial(double num)
        {
            if (num == 1)
            {
                return 1;
            }
            else
            {
                return num * Factorial(num - 1);
            }
        }

        private void FindSumBtn_Click(object sender, EventArgs e)
        {
            double sum = 0;
            double a = Convert.ToDouble(realNumTextBox.Text);
            double n = Convert.ToDouble(naturalNumTextBox.Text);
            denom = a;
            for (int i = 1; i <= n; i++)
            {
                numerat = Factorial(i);
                for (int j = 0; j < i; j++)
                {
                    denom *= a + j;
                }
                sum += numerat / denom;
            }
            resultTextBox.Text = sum.ToString();


        }
    }
}
