﻿namespace Lab21C_
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            realNumLabel = new Label();
            realNumTextBox = new TextBox();
            naturalNumTextBox = new TextBox();
            naturalNumLabel = new Label();
            pictureBox1 = new PictureBox();
            FindSumBtn = new Button();
            resultLabel = new Label();
            resultTextBox = new TextBox();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            SuspendLayout();
            // 
            // realNumLabel
            // 
            realNumLabel.AutoSize = true;
            realNumLabel.Font = new Font("Segoe UI Semibold", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            realNumLabel.Location = new Point(127, 37);
            realNumLabel.Name = "realNumLabel";
            realNumLabel.Size = new Size(172, 25);
            realNumLabel.TabIndex = 0;
            realNumLabel.Text = "Type a real number";
            // 
            // realNumTextBox
            // 
            realNumTextBox.Location = new Point(459, 23);
            realNumTextBox.Name = "realNumTextBox";
            realNumTextBox.Size = new Size(114, 27);
            realNumTextBox.TabIndex = 1;
            // 
            // naturalNumTextBox
            // 
            naturalNumTextBox.Location = new Point(459, 83);
            naturalNumTextBox.Name = "naturalNumTextBox";
            naturalNumTextBox.Size = new Size(114, 27);
            naturalNumTextBox.TabIndex = 3;
            // 
            // naturalNumLabel
            // 
            naturalNumLabel.AutoSize = true;
            naturalNumLabel.Font = new Font("Segoe UI Semibold", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            naturalNumLabel.Location = new Point(114, 85);
            naturalNumLabel.Name = "naturalNumLabel";
            naturalNumLabel.Size = new Size(200, 25);
            naturalNumLabel.TabIndex = 2;
            naturalNumLabel.Text = "Type a natural number";
            // 
            // pictureBox1
            // 
            pictureBox1.BorderStyle = BorderStyle.FixedSingle;
            pictureBox1.Image = (Image)resources.GetObject("pictureBox1.Image");
            pictureBox1.Location = new Point(46, 145);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(703, 132);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.TabIndex = 4;
            pictureBox1.TabStop = false;
            // 
            // FindSumBtn
            // 
            FindSumBtn.BackColor = Color.WhiteSmoke;
            FindSumBtn.Font = new Font("Arial Black", 12F, FontStyle.Bold, GraphicsUnit.Point);
            FindSumBtn.Location = new Point(226, 346);
            FindSumBtn.Name = "FindSumBtn";
            FindSumBtn.Size = new Size(292, 70);
            FindSumBtn.TabIndex = 5;
            FindSumBtn.Text = "Find the Sum";
            FindSumBtn.UseVisualStyleBackColor = false;
            FindSumBtn.Click += FindSumBtn_Click;
            // 
            // resultLabel
            // 
            resultLabel.AutoSize = true;
            resultLabel.Font = new Font("Arial Black", 16.2F, FontStyle.Bold, GraphicsUnit.Point);
            resultLabel.Location = new Point(176, 495);
            resultLabel.Name = "resultLabel";
            resultLabel.Size = new Size(123, 40);
            resultLabel.TabIndex = 6;
            resultLabel.Text = "Result:";
            // 
            // resultTextBox
            // 
            resultTextBox.BackColor = SystemColors.ButtonHighlight;
            resultTextBox.Location = new Point(347, 506);
            resultTextBox.Name = "resultTextBox";
            resultTextBox.Size = new Size(200, 27);
            resultTextBox.TabIndex = 7;
            // 
            // Form2
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 631);
            Controls.Add(resultTextBox);
            Controls.Add(resultLabel);
            Controls.Add(FindSumBtn);
            Controls.Add(pictureBox1);
            Controls.Add(naturalNumTextBox);
            Controls.Add(naturalNumLabel);
            Controls.Add(realNumTextBox);
            Controls.Add(realNumLabel);
            Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            Name = "Form2";
            Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label realNumLabel;
        private TextBox realNumTextBox;
        private TextBox naturalNumTextBox;
        private Label naturalNumLabel;
        private PictureBox pictureBox1;
        private Button FindSumBtn;
        private Label resultLabel;
        private TextBox resultTextBox;
    }
}