﻿namespace Lab21C_
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            base1Label = new Label();
            base1TextBox = new TextBox();
            num1TextBox = new TextBox();
            num1Label = new Label();
            base2TextBox = new TextBox();
            base2Label = new Label();
            num2TextBox = new TextBox();
            num2Label = new Label();
            decimNum1Label = new Label();
            decimNum2Label = new Label();
            CompareBtn = new Button();
            compareSighLabel = new Label();
            descriptionLabel = new Label();
            SuspendLayout();
            // 
            // base1Label
            // 
            base1Label.AutoSize = true;
            base1Label.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            base1Label.Location = new Point(12, 54);
            base1Label.Name = "base1Label";
            base1Label.Size = new Size(55, 28);
            base1Label.TabIndex = 0;
            base1Label.Text = "Base:";
            // 
            // base1TextBox
            // 
            base1TextBox.Location = new Point(92, 52);
            base1TextBox.Name = "base1TextBox";
            base1TextBox.Size = new Size(48, 27);
            base1TextBox.TabIndex = 1;
            // 
            // num1TextBox
            // 
            num1TextBox.Location = new Point(274, 52);
            num1TextBox.Name = "num1TextBox";
            num1TextBox.Size = new Size(136, 27);
            num1TextBox.TabIndex = 3;
            // 
            // num1Label
            // 
            num1Label.AutoSize = true;
            num1Label.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            num1Label.Location = new Point(170, 54);
            num1Label.Name = "num1Label";
            num1Label.Size = new Size(92, 25);
            num1Label.TabIndex = 2;
            num1Label.Text = "Number 1";
            // 
            // base2TextBox
            // 
            base2TextBox.Location = new Point(92, 120);
            base2TextBox.Name = "base2TextBox";
            base2TextBox.Size = new Size(48, 27);
            base2TextBox.TabIndex = 5;
            // 
            // base2Label
            // 
            base2Label.AutoSize = true;
            base2Label.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            base2Label.Location = new Point(12, 120);
            base2Label.Name = "base2Label";
            base2Label.Size = new Size(55, 28);
            base2Label.TabIndex = 4;
            base2Label.Text = "Base:";
            // 
            // num2TextBox
            // 
            num2TextBox.Location = new Point(274, 121);
            num2TextBox.Name = "num2TextBox";
            num2TextBox.Size = new Size(136, 27);
            num2TextBox.TabIndex = 7;
            // 
            // num2Label
            // 
            num2Label.AutoSize = true;
            num2Label.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            num2Label.Location = new Point(170, 123);
            num2Label.Name = "num2Label";
            num2Label.Size = new Size(92, 25);
            num2Label.TabIndex = 6;
            num2Label.Text = "Number 2";
            // 
            // decimNum1Label
            // 
            decimNum1Label.AutoSize = true;
            decimNum1Label.Font = new Font("Segoe UI", 16.2F, FontStyle.Bold, GraphicsUnit.Point);
            decimNum1Label.Location = new Point(83, 312);
            decimNum1Label.Name = "decimNum1Label";
            decimNum1Label.Size = new Size(0, 38);
            decimNum1Label.TabIndex = 8;
            // 
            // decimNum2Label
            // 
            decimNum2Label.AutoSize = true;
            decimNum2Label.Font = new Font("Segoe UI", 16.2F, FontStyle.Bold, GraphicsUnit.Point);
            decimNum2Label.Location = new Point(268, 312);
            decimNum2Label.Name = "decimNum2Label";
            decimNum2Label.Size = new Size(0, 38);
            decimNum2Label.TabIndex = 9;
            decimNum2Label.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // CompareBtn
            // 
            CompareBtn.Font = new Font("Verdana", 9F, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point);
            CompareBtn.Location = new Point(156, 225);
            CompareBtn.Name = "CompareBtn";
            CompareBtn.Size = new Size(94, 29);
            CompareBtn.TabIndex = 10;
            CompareBtn.Text = "Compare ";
            CompareBtn.UseVisualStyleBackColor = true;
            CompareBtn.Click += CompareBtn_Click;
            // 
            // compareSighLabel
            // 
            compareSighLabel.AutoSize = true;
            compareSighLabel.Font = new Font("Segoe UI", 16.2F, FontStyle.Bold, GraphicsUnit.Point);
            compareSighLabel.Location = new Point(182, 312);
            compareSighLabel.Name = "compareSighLabel";
            compareSighLabel.Size = new Size(0, 38);
            compareSighLabel.TabIndex = 11;
            // 
            // descriptionLabel
            // 
            descriptionLabel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            descriptionLabel.AutoSize = true;
            descriptionLabel.BorderStyle = BorderStyle.FixedSingle;
            descriptionLabel.Font = new Font("Segoe UI Semibold", 9F, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point);
            descriptionLabel.Location = new Point(12, 9);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new Size(310, 22);
            descriptionLabel.TabIndex = 12;
            descriptionLabel.Text = "You can type bases from 2 to 10 and also 16.";
            descriptionLabel.TextAlign = ContentAlignment.TopCenter;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(422, 390);
            Controls.Add(descriptionLabel);
            Controls.Add(compareSighLabel);
            Controls.Add(CompareBtn);
            Controls.Add(decimNum2Label);
            Controls.Add(decimNum1Label);
            Controls.Add(num2TextBox);
            Controls.Add(num2Label);
            Controls.Add(base2TextBox);
            Controls.Add(base2Label);
            Controls.Add(num1TextBox);
            Controls.Add(num1Label);
            Controls.Add(base1TextBox);
            Controls.Add(base1Label);
            Name = "Form1";
            Text = "Form1";
            Load += Form1_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label base1Label;
        private TextBox base1TextBox;
        private TextBox num1TextBox;
        private Label num1Label;
        private TextBox base2TextBox;
        private Label base2Label;
        private TextBox num2TextBox;
        private Label num2Label;
        private Label decimNum1Label;
        private Label decimNum2Label;
        private Button CompareBtn;
        private Label compareSighLabel;
        private Label descriptionLabel;
    }
}