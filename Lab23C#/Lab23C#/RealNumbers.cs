﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab23C_
{
    internal class RealNumbers
    {
       public static double[] GetRandomNumbers(int amount, double minValue = -10, double maxValue= 10) 
       {
            double[] numbers = new double[amount];  
            Random rnd = new Random(); 
            for (int i = 0; i < amount; i++)
            {
                numbers[i] = rnd.NextDouble() * (maxValue-minValue) + minValue;
            }
            return numbers;
       }
       public static void WriteNumbersToGrid(double[] numbers, DataGridView grid)
       {
            grid.RowCount = 1;
            grid.ColumnCount = numbers.Length;
            for(int j  = 0; j < grid.ColumnCount; j++)
            {
                grid[j, 0].Value = numbers[j];
            }     
       }
        public static double[] ReadNumbersFromGrid(DataGridView grid)
        {
            double[] numbers = new double [ grid.ColumnCount ];
            for (int j = 0; j < grid.ColumnCount; j++)
            {
                numbers[j] = Convert.ToDouble(grid[j, 0].Value);
            }
            return numbers;

        }
        public static void FillGridWithRandomNumbers(DataGridView grid) 
        {
            double[] numbers = RealNumbers.GetRandomNumbers(grid.ColumnCount);
            RealNumbers.WriteNumbersToGrid(numbers, grid);

        }
        public static void SaveNumbersToFile(string fileName, double[] numbers)
        {
            StreamWriter sw = new StreamWriter(fileName);
            sw.WriteLine(numbers.Length); // the amount of elements
            for (int i = 0; i < numbers.Length; i++)
            {
                sw.WriteLine(numbers[i]);
            }
            sw.Close();
        }
        public static double[] ReadNumbersFromFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            int amountOfNums = Convert.ToInt32(sr.ReadLine());
            double[] numbers = new double[amountOfNums];
            for (int i = 0;i < amountOfNums; i++)
            {
                numbers[i] = Convert.ToDouble(sr.ReadLine());
            }
            sr.Close();
            return numbers;
        }
        public static void SaveGridToFile(string fileName, DataGridView grid)
        {
            double[] numbers = ReadNumbersFromGrid(grid);
            SaveNumbersToFile(fileName, numbers);
        }
        public static void ReadGridDataFromFile(string fileName, DataGridView grid)
        {
            double[] numbers = ReadNumbersFromFile(fileName);
            WriteNumbersToGrid(numbers, grid);
        }
        public static double FindLargestNegativeNumber(DataGridView grid)
        {
            double[] numbers = ReadNumbersFromGrid(grid);
            double largestNeg = int.MinValue;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] < 0)
                {
                    if (numbers[i] > largestNeg)
                    {
                        largestNeg = numbers[i];
                    }
                }
            }
            return largestNeg;
        }

    }
}
