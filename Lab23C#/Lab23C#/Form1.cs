using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab23C_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = (int)numericUpDown1.Value;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = (int)numericUpDown1.Value;
        }

        private void generateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RealNumbers.FillGridWithRandomNumbers(dataGridView1);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) // to save data to file 
        {
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                RealNumbers.SaveGridToFile(saveFileDialog1.FileName, dataGridView1);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                RealNumbers.ReadGridDataFromFile(openFileDialog1.FileName, dataGridView1 );
                numericUpDown1.Value = dataGridView1.ColumnCount;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void largestAmongNegativesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double largestNeg = RealNumbers.FindLargestNegativeNumber(dataGridView1);
            if(largestNeg  == int.MinValue)
            {
                MessageBox.Show("There is no negative number", "The Largest negative number");
            }
            else
            {
                MessageBox.Show( largestNeg.ToString(), "The Largest negative number");
            }
        }
    }
}
